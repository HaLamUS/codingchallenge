//
//  UIButtonExtension.swift
//  CodingChallenge
//
//  Created by Ha Lam on 4/30/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import UIKit

extension UIButton {
  
  func titleAndImageWith(spacing: CGFloat) {
    let insetAmount = spacing / 2
    imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
    titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
    contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
  }
  
  func changeTitleWithoutAnimation(title: String?) {
    UIView.setAnimationsEnabled(false)
    setTitle(title, for: .normal)
    layoutIfNeeded()
    UIView.setAnimationsEnabled(true)
  }
  
  
}
