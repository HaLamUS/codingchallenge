//
//  DateExtension.swift
//  CodingChallenge
//
//  Created by Ha Lam on 5/2/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import UIKit

extension Date {
  //2016-01-10T00:00:08+00:00
  static func jsonStringToDate(dateString:String) -> Date? {
    let format = DateFormatter()
    format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss+zz:zz"
    format.timeZone = NSTimeZone(name: "UTC") as TimeZone!
    return format.date(from: dateString)
  }
  
  func postFormat() -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd 'at 'HH:mm"
    return dateFormatter.string(from: self)
  }
  
}
