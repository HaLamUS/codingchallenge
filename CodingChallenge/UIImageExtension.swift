//
//  UIImageExtension.swift
//  CodingChallenge
//
//  Created by Ha Lam on 5/8/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import UIKit

extension UIImage {
  
  func saveImage(name: String) {
    guard let dataImage = UIImageJPEGRepresentation(self, 0.8) else {return}
    let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let nameImage = name + ".jpg"
    let writePath = path.appendingPathComponent(nameImage)
    try? dataImage.write(to: writePath)
    
  }
  
  static func loadImageFromPath(name: String) -> UIImage? {
    let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let writePath = path.appendingPathComponent(name)
    guard let image = UIImage(contentsOfFile: writePath.path) else {
      return nil}
    return image
  }
  
  
}
