//
//  PostModel.swift
//  CodingChallenge
//
//  Created by Ha Lam on 5/1/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import Foundation

struct PostModel {
  let id: Int
  let fullName: String
  let avatar: String
  let message: String
  let postedAt: String
  let imagePost: String
  var likes: Int
  var comments: Int
  
}

extension PostModel {
  
  static let myGroup = DispatchGroup()
  static var likes: [Int: Int] = [:] // use class will better
  static var comments: [Int: Int] = [:]
  
  init?(json: [String: Any]) {
    guard let id = json["_id"] as? Int,
      let message = json["message"] as? String,
      let postedAt = json["postedAt"] as? String,
      let imagePost = json["imageURL"] as? String
      else {
        return nil
    }
    
    guard let user = json["user"] as? [String: Any]
      else {
        return nil
    }
    
    guard let fullName = user["fullName"] as? String,
      let avatar = user["imageURL"] as? String else {
        return nil
    }
    
    self.id = id
    self.message = message
    self.postedAt = postedAt
    self.imagePost = imagePost
    self.likes = 0
    self.comments = 0
    self.fullName = fullName
    self.avatar = avatar
    
  }
  
  init(dict: [String: Any]) {
    id = dict["id"] as! Int
    message = dict["message"] as! String
    postedAt = dict["postedAt"] as! String
    imagePost = dict["imagePost"] as! String
    likes = dict["likes"] as! Int
    comments = dict["comments"] as! Int
    fullName = dict["fullName"] as! String
    avatar = dict["avatar"] as! String
  }
  
  func encoded() -> [String: Any] {
    var dict = [String: Any]()
    dict["id"] = id
    dict["message"] = message
    dict["postedAt"] = postedAt
    dict["imagePost"] = imagePost
    dict["likes"] = likes
    dict["comments"] = comments
    dict["fullName"] = fullName
    dict["avatar"] = avatar
    return dict
  }
  
}

extension PostModel: Serialization {
  
  static func parse(data: [String: Any]) {
    guard let posts = data["data"] as? [[String : Any]] else {return}
    for post in posts {
      if let model = PostModel(json: post) {
        
        getLikeForPost(postId: model.id)
        getCommentForPost(postId: model.id)
        
        Constant.Api.posts.append(model)
      }
    }
    updateResult()
  }
  
  static func getLikeForPost(postId: Int) {
    myGroup.enter()
    let likeCountUrl = Constant.Api.BaseUrl + "post/\(postId)/likeCount"
    NetWork.get(likeCountUrl, completion: {
      data, error in
      guard let data = data as? [String: Any] else {return}
      guard let likeCounts = data["data"] as? Int else {return}
      likes[postId] = likeCounts
      myGroup.leave()
    })
  }
  
  static func getCommentForPost(postId: Int) {
    myGroup.enter()
    let commentCountUrl = Constant.Api.BaseUrl + "post/\(postId)/commentCount"
    NetWork.get(commentCountUrl, completion: {
      data, error in
      guard let data = data as? [String: Any] else {return}
      guard let commentCounts = data["data"] as? Int else {return}
      comments[postId] = commentCounts
      myGroup.leave()
    })
  }
  
  static func updateResult() {
    myGroup.notify(queue: .global()) {
      for i in Constant.Api.posts.indices {
        Constant.Api.posts[i].likes = likes[Constant.Api.posts[i].id]!
        Constant.Api.posts[i].comments = comments[Constant.Api.posts[i].id]!
      }
      NotificationCenter.default.post(name: Notification.Name(Constant.Notification.Data), object: nil)
    }
  }
  
  
}

struct PostRequest: Request {
  let parameter: [String : AnyObject] = [:]
  let path: String = "post"
  let method: HTTPMethod = .GET
  typealias Response = PostModel
  
}




















