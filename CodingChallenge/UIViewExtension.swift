//
//  UIViewExtension.swift
//  CodingChallenge
//
//  Created by Ha Lam on 4/29/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import UIKit

extension UIView {
  
  func addConstraintsWithFormat(format: String, views: UIView ...) {
    var viewsDictionary = [String: UIView]()
    for (index, view) in views.enumerated() {
      let key = "v\(index)"
      view.translatesAutoresizingMaskIntoConstraints = false
      viewsDictionary[key] = view
    }
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    
    
  }
  
  
}

