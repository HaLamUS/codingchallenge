//
//  DownloadImage.swift
//  CodingChallenge
//
//  Created by Ha Lam on 5/2/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import UIKit

protocol DownloadImageDelegate:class {
  func downloadPhotoDidFinish(_ operation: DownloadImage, image: UIImage)
  func downloadPhotoDidFail(_ operation: DownloadImage)
}

class DownloadImage: Operation {
  let key: String
  let photoURL: String
  let photoName: String
  weak var delegate: DownloadImageDelegate?
  
  init(key: String, photoURL: String, delegate: DownloadImageDelegate) {
    self.key = key
    self.photoURL = photoURL
    self.delegate = delegate
    self.photoName = (photoURL.components(separatedBy: "/").last?.components(separatedBy: ".").first)!
  }
  
  override func main() {
    if self.isCancelled {
      return
    }
    let imageData = try? Data(contentsOf: URL(string: photoURL)!)
    guard let _ = imageData else {
      return
    }
    if let downloadImage = UIImage(data:imageData!) {
      downloadImage.saveImage(name: photoName)
      DispatchQueue.main.async {
        self.delegate?.downloadPhotoDidFinish(self, image: downloadImage)
      }
    }
    else{
      DispatchQueue.main.async {
        self.delegate?.downloadPhotoDidFail(self)
      }
    }
  }
}







