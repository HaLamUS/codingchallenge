//
//  Network.swift
//  CodingChallenge
//
//  Created by Ha Lam on 5/1/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
  case GET
  case POST
  case PUT
  case DELETE
}

enum Result<T> {
  case success(T)
  case failure(Error?)
}

protocol Serialization {
  static func parse(data: [String: Any])
}

protocol Request {
  var path: String { get }
  var method: HTTPMethod { get }
  var parameter: [String:AnyObject] { get }
  associatedtype Response: Serialization
}

protocol RequestSender {
  
  var host: String { get }
  func send<T: Request>(_ r: T, handler: @escaping (Result<AnyObject?>) ->())
}

extension RequestSender {
  
  var host: String {
    return Constant.Api.BaseUrl
  }
}

struct URLSessionRequestSender: RequestSender {
  
  func send<T : Request>(_ r: T, handler: @escaping (Result<AnyObject?>) ->()) {
    let url = URL(string: host.appending(r.path))!
    var request = URLRequest(url: url)
    request.httpMethod = r.method.rawValue
    if r.parameter.count > 0 {
      let jsonData = try? JSONSerialization.data(withJSONObject: r.parameter, options: [])
      request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      request.httpBody = jsonData
    }
    let task = URLSession.shared.dataTask(with: request) {
      data, res, error in
      guard error == nil else {
        print("Error \(error!)")
        handler(.failure(error))
        return
      }
      let httpRes = res as! HTTPURLResponse
      guard httpRes.statusCode == 200 else {
        print("Error http header \(httpRes.statusCode)")
        handler(.failure(nil))
        return
      }
      if let data = data,
        let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
        T.Response.parse(data: json!)
        handler(.success(json as AnyObject))
      } else {
        handler(.success(nil))
      }
    }
    task.resume()
  }
  
  
}

struct NetWork {
  
  static func get(_ url:String, completion: @escaping (Any,Error?)-> ()) {
    let requestURL:URL = URL(string: url)!
    URLSession.shared.dataTask(with: requestURL) {
      (data, response, error) in
      guard error == nil else { print("Co loi roi \(error!)"); return }
      let httpRes = response as! HTTPURLResponse
      guard httpRes.statusCode == 200 else {
        completion([], error); print( "Loi \(httpRes.statusCode)"); return
      }
      guard let data = data,
        let inboxData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) else {
          print("Not json type"); return
      }
      completion(inboxData,error)
      }.resume()
  }
}































