//
//  Global.swift
//  CodingChallenge
//
//  Created by Ha Lam on 5/1/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import Foundation

struct Constant {
  struct Api {
    static let BaseUrl = "http://thedemoapp.herokuapp.com/"
    static var posts: [PostModel] = [] // will refact later
    static var postsInstance: [PostModel] = []
  }
  struct Notification {
    static let Data = "DataIsReady"
  }
}
