//
//  MomentsViewController.swift
//  CodingChallenge
//
//  Created by Ha Lam on 4/29/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import UIKit

let cellId = "cell"
let cellWidth = UIScreen.main.bounds.width - 40
let cellHeight = ((UIScreen.main.bounds.width - 40) * 9 / 16) + 90
let cellSpacing: CGFloat = 20.0

class MomentsViewController: UIViewController {
  
  //MARK: @IBOutlet
  
  @IBOutlet weak var momentsCollectionView: UICollectionView! {
    didSet {
      momentsCollectionView.delegate = self
      momentsCollectionView.dataSource = self
      let layout = UICollectionViewFlowLayout()
      layout.sectionInset = UIEdgeInsets(top: cellSpacing, left: cellSpacing, bottom: cellSpacing, right: cellSpacing - 10)
      layout.minimumLineSpacing = cellSpacing
      momentsCollectionView.collectionViewLayout = layout
    }
  }
  
  @IBOutlet weak var reloadButton: UIButton! {
    didSet {
      reloadButton.layer.cornerRadius = 8
      reloadButton.layer.masksToBounds = true
    }
  }
  
  //MARK: @IBAction
  
  @IBAction func touchUpReloadButton(_ sender: UIButton) {
    sender.isHidden = true
    posts = Constant.Api.posts
    DispatchQueue.main.async {
      self.momentsCollectionView.reloadData()
    }
  }
  
  //MARK: Properties
  
  var cacheImage: [String: UIImage] = [:]
  var downloadingTasks: [String: Operation] = [:]
  lazy var downLoadPhotoQueue: OperationQueue = {
    $0.name = "Download Photo"
    $0.maxConcurrentOperationCount = 2
    return $0
  }(OperationQueue())
  var posts: [PostModel] = []
  
  //MARK: View Life Circle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    removeBottomLineNavigationBar()
    NotificationCenter.default.addObserver(self, selector: #selector(updateNewData), name: Notification.Name(Constant.Notification.Data), object: nil)
    guard Constant.Api.postsInstance.count > 0 else {
      showLoading()
      getPosts()
      return
    }
    posts = Constant.Api.postsInstance
    getPosts()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
}

extension MomentsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return posts.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MomentsCollectionViewCell
    let post = posts[indexPath.row]
    cell.post = post
    if let imageForCell = loadImageForCell(id: "\(post.id)image", imageUrl: post.imagePost) {
      cell.postImageView.image = imageForCell
    }
    
    if let avatarForCell = loadImageForCell(id: "\(post.id)avatar", imageUrl: post.avatar) {
      cell.avataImageView.image = avatarForCell
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: cellWidth, height: cellHeight)
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    guard let scrollView = scrollView as? UICollectionView else {
      return
    }
    if scrollView.isDecelerating {
      downLoadPhotoQueue.cancelAllOperations()
      downloadingTasks.removeAll()
    }
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    momentsCollectionView.reloadData()
  }
  
  
}

extension MomentsViewController {
  
  func removeBottomLineNavigationBar() {
    navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationController?.navigationBar.shadowImage = UIImage()
  }
  
  func getPosts() {
    let urlSession = URLSessionRequestSender()
    urlSession.send(PostRequest()) { // inject test able
      [weak self] result in
      switch result {
      case .success(_):
        DispatchQueue.main.async {
          self?.momentsCollectionView.reloadData()
        }
      case .failure(let error):
        print(error!)
      }
    }
  }
  
  func loadImageForCell(id: String, imageUrl: String) -> UIImage? {
    let imageName = (imageUrl.components(separatedBy: "/").last?.components(separatedBy: ".").first)! + ".jpg"
    if let imageInMem  = cacheImage[imageUrl] {
      return imageInMem
    }
    else if let imageInLocal = UIImage.loadImageFromPath(name: imageName) {
      return imageInLocal
    }
    else {
      if !momentsCollectionView.isDecelerating {
        let photoDownloader = DownloadImage(key: id, photoURL: imageUrl, delegate: self)
        startDownloadPhoto(photoDownloader)
      }
    }
    return nil
  }
  
  func startDownloadPhoto(_ downloader: DownloadImage) {
    if (downloadingTasks[downloader.key] != nil) { return }
    downLoadPhotoQueue.addOperation(downloader)
    downloadingTasks[downloader.key] = downloader
  }
  
  func updateNewData() {
    posts = Constant.Api.posts
    if Constant.Api.postsInstance.count > 0 {
      DispatchQueue.main.async {
        self.reloadButton.isHidden = false
      }
    }
    else {
      DispatchQueue.main.async {
        self.dissmissLoading()
        self.momentsCollectionView.reloadData()
      }
    }
  }
  
}


extension MomentsViewController: DownloadImageDelegate {
  
  func downloadPhotoDidFinish(_ operation: DownloadImage, image: UIImage) {
    cacheImage[operation.photoURL] = image
    downloadingTasks.removeValue(forKey: operation.key)
    momentsCollectionView.reloadData()
  }
  func downloadPhotoDidFail(_ operation: DownloadImage) {
    cacheImage[operation.photoURL] = #imageLiteral(resourceName: "story") // temporary at the moment
    downloadingTasks.removeValue(forKey: operation.key)
    momentsCollectionView.reloadData()
  }
}













