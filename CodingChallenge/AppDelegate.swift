//
//  AppDelegate.swift
//  CodingChallenge
//
//  Created by Ha Lam on 4/29/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import UIKit

let savePosts = "Moments"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    window?.makeKeyAndVisible()
    addBackgroundViewStatusBar()
    let posts = UserDefaults.standard.object(forKey: savePosts) as? [[String: Any]] ?? [[String: Any]]()
    Constant.Api.postsInstance = posts.map { PostModel(dict: $0) }
    return true
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    let posts = Constant.Api.posts.map {$0.encoded()}
    UserDefaults.standard.set(posts, forKey: savePosts)
  }
  
}

extension AppDelegate {
  func addBackgroundViewStatusBar() {
    let statusBarBackgroundView = UIView()
    statusBarBackgroundView.backgroundColor = UIColor.rgb(red: 161, green: 161, blue: 161)
    window?.addSubview(statusBarBackgroundView)
    
    window?.addConstraintsWithFormat(format: "H:|[v0]|", views: statusBarBackgroundView)
    window?.addConstraintsWithFormat(format: "V:|[v0(20)]", views: statusBarBackgroundView)
    
  }
}
























