//
//  UIViewControllerExtension.swift
//  CodingChallenge
//
//  Created by Ha Lam on 5/15/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import UIKit

extension UIViewController {
  
  func showLoading() {
    let activityIndicatoryView = UIActivityIndicatorView()
    activityIndicatoryView.tag = 1993
    
    activityIndicatoryView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    activityIndicatoryView.center = view.center
    activityIndicatoryView.hidesWhenStopped = true
    activityIndicatoryView.activityIndicatorViewStyle = .whiteLarge
    view.addSubview(activityIndicatoryView)
    activityIndicatoryView.backgroundColor = UIColor.rgb(red: 128, green: 128, blue: 128, alpha: 0.5)
    activityIndicatoryView.startAnimating()
  }
  
  func dissmissLoading() {
    let activityIndicatoryView = view.viewWithTag(1993)
    activityIndicatoryView?.removeFromSuperview()
  }
}
