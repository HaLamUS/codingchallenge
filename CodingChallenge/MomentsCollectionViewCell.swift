//
//  MomentsCollectionViewCell.swift
//  CodingChallenge
//
//  Created by Ha Lam on 4/30/17.
//  Copyright © 2017 Gotadi. All rights reserved.
//

import UIKit

class MomentsCollectionViewCell: UICollectionViewCell {
  
  //MARK: @IBOutlet
  
  @IBOutlet weak var avataImageView: UIImageView! {
    didSet {
      avataImageView.layer.cornerRadius = avataImageView.bounds.height / 2
      avataImageView.layer.masksToBounds = true
    }
  }
  
  @IBOutlet weak var nameLabel: UILabel! {
    didSet {
      nameLabel.numberOfLines = 2
    }
  }
  
  @IBOutlet weak var likeButton: UIButton! {
    didSet {
      likeButton.tintColor = .black
      likeButton.setImage(#imageLiteral(resourceName: "icon_moments_like_black"), for: .normal)
      likeButton.titleAndImageWith(spacing: 8)
    }
  }
  
  @IBOutlet weak var commentButton: UIButton! {
    didSet {
      commentButton.tintColor = .black
      commentButton.setImage(#imageLiteral(resourceName: "icon_moments_comment"), for: .normal)
      commentButton.titleAndImageWith(spacing: 8)
    }
  }
  
  @IBOutlet weak var messageLabel: UILabel!
  
  @IBOutlet weak var postImageView: UIImageView!
  
  //MARK: Properties
  
  var post: PostModel? {
    didSet {
      let name = NSMutableAttributedString(string: "\(post!.fullName) \n", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)])
      let time = Date.jsonStringToDate(dateString: post!.postedAt)?.postFormat()
      let timePosted = NSMutableAttributedString(string: time!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 12)])
      name.append(timePosted)
      nameLabel.attributedText = name
      messageLabel.text = post?.message
      likeButton.changeTitleWithoutAnimation(title: "\(post!.likes)")
      commentButton.changeTitleWithoutAnimation(title: "\(post!.comments)")
    }
  }
  
  
}











