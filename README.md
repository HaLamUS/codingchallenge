# Coding Challenge - LamHa #

Social network written in Swift

### Require ###
* Xcode 8.3.2
* iOS 10.3 above


### Set up ###
1. Pull code
1. Build with simulator


### Some notes ###

Network Architect:

* You can "plug and play" by implementing any struct with protocol 'RequestSender'.

* You can change URLSession to Alamofire or anything.

* You also can mock test with protocol 'Request'
* Totally Dependency Injection

Cache Data:

* Offline (include image)
* Notify for user when new data is ready

Demo project:

https://www.youtube.com/watch?v=Fr9hf6d34Tc

### Author ###
LamHa